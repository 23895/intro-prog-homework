#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 14 19:07:18 2018

@author: anajardim
"""

#Exercise 1
tup=(1,2,3) 
tup[0]=5 #gives an error

#Exercise 3
tup.sort(reverse=false) #no attribute

#Exercise 4
my_list=(1000, 0.9, 890, 450, 789, 6.9, 345)
my_list.append(876)

#Exercise 9
e={'hello': 'world'}
e['Goodbye'] = 'Schhol'

#Exercise 10 - 100
stock_prices = {'APPL': 100, 'GOOG': 99}
stock_of_interest='APPL'
print (stock_prices[stock_of_interest])

#Exercise 11
#[1,2,3] or list((1,2,3))
#[1,2,3] or tuple([1,2,3])
#d={...} or dict(a=10)

#Exercise 12
a_dict={
        'a_key': 1,
        'other_key': 2
}

print(a_dict)

del a_dict[a_key] #delete the first key
print(a_dict)

a_dict.pop('other_key') #delete the second key
print (a_dict)

#Exercise 13

def function(to_list):
    to_list.append('homie')
    return to_list

#To merge two dictionaires one can use the function a_dict.update()
    



         
