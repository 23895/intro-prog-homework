#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 16:37:42 2018

@author: anajardim
"""

#Exercise 1

class LectureRoom:
    def __init__ (self):
        self.capacity = 40
        self.min_capacity = 40
        self.max_capacity = 100
        
    def valid_capacity (self, new_capacity):
        if new_capacity < self.min_capacity:
            print ('invalid capacity, must be between 40 and 100')
            return False
            
        elif new_capacity > self.max_capacity:
            print ('invalid capacity, must be between 40 and 100')
            return False
        
        else:
            return True
    
    def increase_capacity (self, amount):
        if self.valid_capacity (self.capacity + amount):
            self.capacity += amount
        
    def decrease_capacity (self, amount):
        if self.valid_capacity (self.capacity - amount):
            self.capacity -= amount
    
    
class BigRoom (LectureRoom):
    def __init__ (self):
        self.capacity = 40
        self.min_capacity = 50
        self.max_capacity = 200

#By defining a new child, the child acquires the skills of the parents
#Super function is only to call a specfic function

class SmallRoom (LectureRoom):
    def __init__ (self):
        self.capacity = 10
        self.min_capacity = 5
        self.max_capacity = 20

#Exercise 2

class Robot:
    def __init__ (self):
        self.recordings = []
        
    def listen (self, string):
        self.recordings.append (string)
    
    def play_recordings (self):
        print (self.recordings)
        
    def delete_recordings (self):
        self.recordings = []

class FlameThrower (Robot): #From during class
    def throw_flames (self):
        print ('Burn, baby! Burn!!!')
        self.delete_recordings()

class Tank (FlameThrower):
    def throw_flames (self):
        print('Throws VERY LARGE flame')
        
    def hulk_stomp (self):
        print ('Hulk stomping everywhere!')

#Exercise 3
        
class LightRobot (Robot):
    def listen (self, string):
        super().listen(string)
        
        if len(self.recordings) >= 3:
            del self.recordings[0]

class FlameThrower(LightRobot):
    pass

class CryingRobot(LightRobot):
    pass

class FlyingRobot(LightRobot):
    pass
        
    
    
    
    
    