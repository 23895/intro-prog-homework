#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 18:22:52 2018

@author: anajardim
"""

if True:
    print('hello world')
    
def return_true():
    return True

if return_true():
    print('hello world')

val = return_true()

if val:
    print ('hello world')
    
if True and True: #because both sides of the and are true
    print('This will be printed')

if True and False: #just one side of the AND needs to be true
    print('This will NOT be printed')

if True or True:
    print('at least one of the things is true')
    print ('This will be printed')

if True or False:
    print('at least one of the things is true')
    print ('This will be printed')

a=True
b=False

if b or b:
    print('at least one thing is true')
    
if b or b or b or b or a:
    print ('at least one thing is true')
    
if True: 
    print('number one, baby!')
elif True:
    print('number 2, baby!')
elif True:
    print('number 3, baby!')
    
if True:
    print ('number one, baby!')
if True: 
    print('number 2, baby!')
if True:
    print('number 3, baby!')


a_list=[1,2,3]
for num in a_list: #It prints every number which is in the referred list 
    print (num)

num=-1
print (num)

for num in a_list:
    print (num)

print(num) #prints the last number on the a_list once again
# why???

a_dict= {
        'APPL': 1,
        'GOOG': 2,
        'FB': 3}

for key in a_dict:
    print('key', key, 
          'value', a_dict[key])

#Homework:
    
#Exercise 1:
    #Empty string is false
    
#Exercise 2:
def ticker_value (ticker):
    print (ticker == 'APPL')
    
#Exercise 3:
def gender_assess (gender):
    if gender == 'female':
        print ('You like pink')
    elif gender == 'male':
        print ('you hate pink')
        
#Exercise 4:
def gender_assess (gender):
    if gender == 'female':
        print ('You like pink')
        
    if gender == 'male':
        print ('you hate pink')
    
    print ('This line shouldn\'t be printed')

#It prints the last statement because it is located within the scope of the function and it is not alocated to none of the if statements 

#Exercise 5:
def ages(age_mother , age_father):
    if age_mother == age_father:
        return age_mother + age_father
    
    else:
        return age_mother - age_father
    
#Exercise 6:
a_list=[1,2,3,4,5,6,7]

a_dict1={
        'ANA': 7,
        'MARIA': 8,
        'JARDIM': 9}

#Exercise 7: 
a_dict= {
        'APPL': 1,
        'GOOG': 2,
        'FB': 3}

for key in a_dict:
    print (key)
    
for key in a_dict:
    print (a_dict[key])

for key in a_dict:
    print( 'the stock price of', key, 'is', a_dict[key])
    
#Exercise 8:
a_tuple=(10,20,30)
ab_list=[40,50,60]

a_dict123={
        'APPL': 100,
        'GOOG': 90,
        'FB': 80}

for num in a_tuple:
    print (num)

for num in ab_list:
    print (num)

for key in a_dict123:
    print(a_dict123[key])
    
#Exercise 9: 
def lista_2(ab_list):
    i=0
    for num in ab_list:
        a_list[i] = num * 2
        i = i + 1
    return ab_list

#As it goes through the loop the number adjacent to i increases within th indexes available

def mult_by_2(ab_list):
    a_new_list = []
    for num in ab_list:
        a_new_list.append(num * 2)
    return a_new_list

#Exercise 10:
def mult_by_2(a_dict):
    a_new_dict={}
    for num in a_dict:
        a_new_dict[key] = a_dict[key] * 2
    return a_new_list

#Exercise 11: 
    

        
    

    
    



