#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov  6 16:36:54 2018

@author: anajardim
"""

#EXERCISE 1

class Robot:
 
    def __init__ (self):
        self.recordings = []
    
    def listen (self, string):
        self.recordings.append(string)
    
    def play_recordings (self):
        print (self.recordings)

        
a = Robot()
b = Robot()

a.listen('alloha')
a.play_recordings()

b.listen('youre awesome')
b.play_recordings()

#They record different things because they are different instances so they experience different things
#They do not share the same recordings array because they are different instances.

#EXERCISE 2

class Robot:
    
    def __init__ (self):
        self.recordings = []
    
    def listen (self, string):
        self.recordings.append(string)
    
    def play_recordings (self):
        print (self.recordings)
    
    def delete_recordings (self):
        self.recordings = []

c = Robot()

c.listen('useless')
c.play_recordings()
c.delete_recordings()
c.play_recordings()
c.listen('useful')
c.play_recordings()

#EXERCISE 3

d = Robot()
e = Robot()

d.listen ('oi')
e.listen ('olá')

d.play_recordings()
# -> ['oi']
e.play_recordings()
# -> ['olá']

d.delete_recordings()

d.play_recordings()
# -> []
e.play_recordings()
# -> ['olá']

#When playing the recordings none of them was able to play because the recordings were deleted
#When deleting one of them, the other one keeps the same because they are different robots

#EXERCISE 4

class LectureRoom:
    
    def __init__ (self):
        self.capacity = 40
        
    def valid_capacity (self, new_capacity):
        if new_capacity < 40:
            print ('invalid capacity, must be between 40 and 100')
            return False
            
        elif new_capacity > 100:
            print ('invalid capacity, must be between 40 and 100')
            return False
        
        else:
            return True
    
    def increase_capacity (self, amount):
        if self.valid_capacity (self.capacity + amount):
            self.capacity += amount
        
    def decrease_capacity (self, amount):
        if self.valid_capacity (self.capacity - amount):
            self.capacity -= amount

a = LectureRoom ()
a.valid_capacity(123)
    